return {
  {
    "lambdalisue/suda.vim",
    lazy = false,
    init = function()
      vim.g["suda#nopass"] = 1
    end,
  },
}
