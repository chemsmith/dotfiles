return {
  -- {
  --   "ellisonleao/gruvbox.nvim",
  --   setup = {
  --     contrast = "hard",
  --   },
  -- },
  -- {
  --   "folke/tokyonight.nvim",
  --   setup = {
  --   },
  -- },
  {
    "catppuccin/nvim",
    name="catppuccin",
    setup = {
    },
  },
  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "catppuccin",
    },
  },
}
