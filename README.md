# dotfiles de chemsmith

My dotfiles, managed by [chezmoi](https://www.chezmoi.io) and [Renovate](https://docs.renovatebot.com/). A [selection of base tooling](./private_dot_config/aqua.yaml.tmpl) is managed with [Aqua](https://aquaproj.github.io/).

## bootstrapping

1. `sh -c "$(curl -fsLS chezmoi.io/get)" -- -b ~/.local/bin/ init --apply gitlab.com/chemsmith/dotfiles` (requires `age` unlock passphrase)
2. ???
3. Profit

## configuration

Host specific configurations can be enabled in `~/.local/share/chezmoi/.chezmoi.yaml.tmpl` by creating a `$hostname` conditional block and setting the relevant flags.

### git ssh keys
1. Add new SSH key from `.ssh/id_<HOSTNAME>.pub` to [github](https://github.com/settings/ssh/new) and [gitlab](https://gitlab.com/-/profile/keys) then edit `~/.local/share/chezmoi/.chezmoi.yaml.tmpl` to enable `gitssh` for hostname eg:
```
{{- if eq $hostname "pallas" }}
{{-   $gitssh = true }}
{{- end }}
```
2. Apply changes that come from enabling git: `chezmoi init && chezmoi apply`
3. Push changes back to here: `chezmoi git -- commit -am "Update .chezmoi.yaml.tmpl" && chezmoi git -- push`

(A similar pattern can be used to enable commit signing (`$gitgpg`). Follow [this](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/#create-a-gpg-key) to generate GnuPG key.)

## Notes

### terminfo

It's possible that some systems won't have the correct terminfo for alacritty (`TERM=alacritty`) or tmux (`TERM=tmux-256color`), both of these are included in the repo and can be installed with `tic -x <terminfo file>`.
