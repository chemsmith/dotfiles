// ==UserScript==
// @name        Auto Vault WebUI - Reauthenticate
// @match       https://vault.cgws.com.au/ui/
// @grant       none
// @version     1.0
// @description Automatically presses reauthenticate link when vault has logged us out
// ==/UserScript==

function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

window.onload = function() {
  sleep(1000).then(() => {
    let reAuthLinks = document.evaluate("//a[contains(., 'Reauthenticate')]", document, null, XPathResult.ANY_TYPE, null );
    let authLink = reAuthLinks.iterateNext();
    if (authLink) {
    	authLink.click()
    }
  });  
}
