// ==UserScript==
// @name        Auto AWS SSO CLI - Login
// @match       https://*.awsapps.com/start/user-consent/authorize.html?clientId=*
// @grant       none
// @description Automatically presses the "Sign in to AWS CLI" button
// ==/UserScript==

window.onload = function() {
  document.getElementById('cli_login_button').click()
}
