// ==UserScript==
// @name        Auto Vault CLI SSO - Close Window
// @match       http://localhost:8250/oidc/*
// @grant       window.close
// @version     1.0
// @description Close the tab when successful Vault SSO CLI login
// ==/UserScript==

setInterval(() => {
  window.close()
}, 1000)

