// ==UserScript==
// @name        Auto Vault WebUI - Login
// @match       https://vault.cgws.com.au/ui/vault/auth?with=azure-oidc%2F
// @grant       none
// @version     1.0
// @description Automatically presses submit on the Vault login page
// ==/UserScript==

function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

window.onload = function() {
sleep(2000).then(() => {
    document.getElementById('auth-submit').click()
});  
}
