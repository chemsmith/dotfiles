// ==UserScript==
// @name        Auto AWS SSO CLI - Accept code
// @match       https://device.sso.ap-southeast-2.amazonaws.com/?user_code=*
// @grant       none
// @version     1.0
// @description Automatically presses the "Confirm and Continue" button
// ==/UserScript==

window.onload = function() {
  document.getElementById('cli_verification_btn').click()
}
