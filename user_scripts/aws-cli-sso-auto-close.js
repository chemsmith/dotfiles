// ==UserScript==
// @name        Auto AWS SSO CLI - Close Window
// @match       https://*.awsapps.com/start/user-consent/login-success.html
// @grant       window.close
// @version     1.0
// @description Close the tab when successful AWS SSO CLI login
// ==/UserScript==

setInterval(() => {
  window.close()
}, 1000)
