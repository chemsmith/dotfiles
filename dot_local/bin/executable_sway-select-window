#!/usr/bin/env bash
# https://gitlab.com/wef/dotfiles/-/blob/master/bin/sway-select-window
# shellcheck disable=SC2034
TIME_STAMP="20230129.084152"

# Copyright (C) 2019-2023 Bob Hepple <bob dot hepple at gmail dot com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

verbose="set"

PROG=$( basename "$0" )

jq_get_windows='
     # descend to workspace or scratchpad
     .nodes[].nodes[]
     # save workspace name as .w
     | {"w": .name} + (
       if (.nodes|length) > 0 then # workspace
         [recurse(.nodes[])]
       else # scratchpad
         []
       end
       + .floating_nodes
       | .[]
       # select nodes with no children (windows)
       | select(.nodes==[])
     )'

jq_windows_to_tsv='
    [
      (.id | tostring),
      # remove markup and index from workspace name, replace scratch with "[S]"
      (.w | gsub("<[^>]*>|:$"; "") | sub("__i3_scratch"; "[S]")),
      # get app name (or window class if xwayland)
      (.app_id // .window_properties.class),
      (.name),
      (.pid)
    ]
    | @tsv'

get_fs_win_in_ws() {
    id="${1:?}"

    swaymsg -t get_tree |
    jq -e -r --argjson id "$id" "
    [ $jq_get_windows  ]
    | (.[]|select(.id == \$id)) as \$selected_workspace
    | .[]
    | select( .w == \$selected_workspace.w and .fullscreen_mode == 1 )
    | $jq_windows_to_tsv
    "
}

focus_window() {
    id="${1}"
    ws_name="${2}"
    app_name="${3}"
    win_title="${4}"

    [[ "$verbose" ]] && printf "focusing window (in workspace %s): [%s] (%s) \`%s\`\n" "$ws_name" "$id" "$app_name" "$win_title" >&2

    # look for fullscreen among other windows in selected window's workspace
    if fs_win_tsv="$( get_fs_win_in_ws "$id" )" ; then
        read -r win_id ws_name app_name win_title <<<"$fs_win_tsv"
        if [ "$win_id" != "$id" ] ; then
            [[ "$verbose" ]] && printf 'found fullscreen window in target workspace (%s): [%s] (%s) "%s"\n' "$ws_name" "$win_id" "$app_name" "$win_title" >&2
            swaymsg "[con_id=$win_id] fullscreen disable"
        fi
    fi

    swaymsg "[con_id=$id]" focus
}

id_fmt='%s'
ws_fmt='%s'
app_fmt='%s'
title_fmt='%s'

swaymsg -t get_tree |
# get list of windows as tab-separated columns
jq -r "$jq_get_windows | $jq_windows_to_tsv" |
# align columns w/ spaces (keep tab as separator)
column -s $'\t' -o $'\t' -t |
# pango format the window list
while IFS=$'\t' read -r win_id ws_name app_name win_title pid; do
    shopt -s extglob
    app_name="${app_name%%*( )}"
    # shellcheck disable=SC2059
    printf "${id_fmt}\t${ws_fmt}\t${app_fmt}\t${title_fmt}\n" \
           "$win_id" \
           "$ws_name" \
           "$app_name" \
           "$win_title"
done |
# sort by workspace name and then app_name:
sort -k2 -k3 |
# defeat the initial cache in order to preserve the order:
wofi --cache-file=/dev/null --insensitive --allow-images --show dmenu --prompt='Focus a window' | {
    FS=$'\t' read -r win_id ws_name app_name win_title
    focus_window "$win_id" "$ws_name" "$app_name" "$win_title"
}

exit 0

