#!/bin/bash
export GPU="0000:01:00.0"
export GPU_AUDIO="0000:01:00.1"

export GPU_PATH="/sys/bus/pci/devices/$GPU/"
export GPU_AUDIO_PATH="/sys/bus/pci/devices/$GPU_AUDIO/"

function _unbind()
{	if [[ -L "${GPU_PATH}driver" ]]; then
		count="$(nvidia-consumers | wc -l)"
		if [[ $count -ge 1 ]]; then
			echo "Things are bound to the nvidia driver, they need to be killed first."
			return 1
		fi
		sudo modprobe -a -r -q nvidia_drm nvidia_modeset nvidia_uvm nvidia || echo "Unable to unload nvidia modules" && return 1
		echo "Unbinding ${GPU}"
		echo "$GPU" | sudo tee "${GPU_PATH}driver/unbind"
	fi

	if [[ -L "${GPU_AUDIO_PATH}driver" ]]; then
		echo "Unbinding ${GPU_AUDIO}"
		echo "$GPU_AUDIO" | sudo tee "${GPU_AUDIO_PATH}/driver/unbind"
	fi

	if [[ -L "${GPU_PATH}driver" || -L "${GPU_AUDIO_PATH}driver" ]]; then
		echo "Unable to unbind driver"
		return 1
	fi
}

function _bind_vfio()
{
	sudo modprobe -q vfio-pci

	echo "vfio-pci" | sudo tee "${GPU_PATH}/driver_override"
	echo "vfio-pci" | sudo tee "${GPU_AUDIO_PATH}/driver_override"

	echo "${GPU}" | sudo tee /sys/bus/pci/drivers_probe
	echo "${GPU_AUDIO}" | sudo tee /sys/bus/pci/drivers_probe

	if [[ ! -L "${GPU_PATH}driver" || ! -L "${GPU_AUDIO_PATH}driver" ]]; then
		echo "Unable to bind vfio-pci"
		return 1
	fi
}

function bind-vfio()
{
	_unbind && _bind_vfio
}

function _bind_nvidia()
{
	echo -n "Removing the GPU... "
	echo 1 | sudo tee "${GPU_PATH}remove"
	echo -n "Removing HDMI Audio... "
	echo 1 | sudo tee "${GPU_AUDIO_PATH}remove"
	echo -n "Rescanning PCI devices... "
	echo 1 | sudo tee /sys/bus/pci/rescan
	modprobe -q nvidia_uvm
}

function bind-nvidia()
{
	_bind_nvidia
}

function nvidia-consumers() {
	sudo lsof /dev/nvidia*
}

function iommu-groups() {
	for d in /sys/kernel/iommu_groups/*/devices/*; do
	  n=${d#*/iommu_groups/*}; n=${n%%/*}
	    printf 'IOMMU Group %s ' "$n"
	      lspci -nns "${d##*/}"
	done
}
