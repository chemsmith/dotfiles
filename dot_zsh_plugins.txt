zdharma-continuum/fast-syntax-highlighting
zsh-users/zsh-autosuggestions
zsh-users/zsh-completions

ohmyzsh/ohmyzsh   path:plugins/colored-man-pages
ohmyzsh/ohmyzsh   path:plugins/sudo

docker/cli         path:contrib/completion/zsh/ kind:fpath
sharkdp/fd         path:contrib/completion/ kind:fpath
ahmetb/kubectx     path:completion kind:fpath
